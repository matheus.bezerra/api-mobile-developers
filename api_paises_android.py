from flask import Flask, jsonify, request
import json
import urllib.request
import random
import paises

app = Flask(__name__)

dic = paises.paises   

@app.route("/paises", methods=['GET'])
def get():
    return jsonify(dic)


@app.route("/paises", methods=['POST'])
def post():
    global dic
    try:
        content = request.get_json()
        
        # gerar id
        ids = [pais["id"] for pais in dic]
        if ids:
            nid = max(ids) + 1
        else:
            nid = 1
        content["id"] = nid
        
        dic.append(content)
        return jsonify({"status":"OK", "msg":"pais adicionado com sucesso"})
    except Exception as ex:
        return jsonify({"status":"ERRO", "msg":str(ex)})

@app.route("/paises/<int:id>", methods=['DELETE'])
def delete(id):
    global dic
    try:
        dic = [pais for pais in dic if pais["id"] != id]
        return jsonify({"status":"OK", "msg":"pais removido com sucesso"})
    except Exception as ex:
        return jsonify({"status":"ERRO", "msg":str(ex)})


if __name__ == "__main__":
    app.run(host='0.0.0.0')
